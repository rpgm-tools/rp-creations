# Coming Soon

-   **Generates JSON for both request and response**
-   Separate API for testing and development
-   Output formatting options (markdown, html, JSON, etc.)
-   High detail options using higher-context AI models
-   Midjourney tokens and portraits?
-   Web app version
-   So much more!
