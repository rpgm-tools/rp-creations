<img src="./img/rp-creations.jpg" alt="Random Procedural Creations">

# Random Procedural Creations - Foundry Module Quick Start Guide - Version 2.0.3

## 1. Introduction

Welcome to the Quick Start Guide for Random Procedural Creations. In the ever-evolving universe of Foundry VTT, creativity fuels every compelling
narrative and immersive gaming experience. This module is designed to help streamline your creative process and enhance the quality of your TTRPG
adventures by generating AI-assisted homebrew content for your worlds.

---

### 1.1 Unleashing Creativity

Our module nurtures your creativity by offering an intelligent solution to homebrew creation, freeing you up to delve into the core elements of
world-building.

### 1.2 Efficiency and Consistency

Random Procedural Creations brings together efficiency and consistency. It is an innovative module designed to reduce the time spent on creating your
homebrew content.

---

## 2. Primary Functions

In this section, we'll discuss the core functions of Random Procedural Creations and how you can make the most of them.

### 2.1 Homebrew Generation

Random Procedural Creations also offers creative homebrew content generation:

- **Chat Commands**: The **!h** or **!home** command creates any kind of homebrew content.

**Examples**: <a href="./img/foundry/tinker.jpg" target=_blank> <img src="./img/foundry/tinker-300.jpg" alt="Tinker"> </a>
<a href="./img/foundry/order-of-mirth.jpg" target=_blank> <img src="./img/foundry/order-of-mirth-300.jpg" alt="Order of Mirth"> </a>
<a href="./img/foundry/ethereal-verge.jpg" target=_blank> <img src="./img/foundry/ethereal-verge-300.jpg" alt="Ethereal Verge"> </a>
<a href="./img/foundry/homebrew-form.jpg" target=_blank> <img src="./img/foundry/homebrew-form-300.jpg" alt="Homebrew Form"> </a>
<a href="./img/foundry/homebrew-form-custom.jpg" target=_blank> <img src="./img/foundry/homebrew-form-custom-300.jpg" alt="Homebrew Form (Custom)">
</a>

**Available Schemas**: Ability, Action, Armor, Artifact, Background, Boss, Building, Class, Condition, CraftingRecipe, Creature, Cult,
CulturalPractice, Currency, Deity, Disease, Drink, Encounter, Event, Faction, Fauna, Feat, Festival, Flora, Food, Guild, HistoricalEvent, Holiday,
Item, Landmark, Language, Legend, Location, MagicItem, Mount, Mythology, NPC, Organization, Plane, PlotHook, Poem, Poison, Profession, Prophecy,
Puzzle, Quest, Race, Relic, Ritual, SecretSociety, Settlement, Shop, Skill, Song, Spell, Subclass, SymbolSigil, Tome, Tradition, Trap, Vehicle, Weapon

### 2.2 Chatbot/General Information

The tool offers information and answers to your questions using ChatGPT:

- **ChatGPT Command**: The **!g** or **!gpt** command in the chat window sends a prompt to ChatGPT. The AI provides a response via whisper in the
  chat.

**Examples**: <a href="./img/foundry/thief-weapon.jpg" target=_blank> <img src="./img/foundry/thief-weapon-300.jpg" alt="Thief Weapon"> </a>
<a href="./img/foundry/nature-barbarian.jpg" target=_blank> <img src="./img/foundry/nature-barbarian-300.jpg" alt="Nature Barbarian"> </a>
<a href="./img/foundry/hex.jpg" target=_blank> <img src="./img/foundry/hex-300.jpg" alt="Hex"> </a>

---

## 3.Settings

See [Random Procedural Core](https://gitlab.com/rpgm-tools/rp-core) for settings.

---

Random Procedural Creations enhances your Foundry VTT experience by enabling a more dynamic, creative, and immersive world-building process. Happy
creating!
