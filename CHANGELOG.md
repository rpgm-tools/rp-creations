# Changelog

All notable changes to the "Random Procedural Creations" module will be documented in this file.

## Important Notes

-

[Coming Soon](https://gitlab.com/rpgm-tools/rp-creations/-/blob/main/FUTUREPLANS.md)

## [2.0.3] - 2023-08-26

### Fixed

- More issues with the homebrew dialog

## [2.0.2] - 2023-08-19

### Fixed

- Errors with creating homebrew content

## [2.0.1] - 2023-08-18

### Fixed

- Misc bug fixes
- Improved load times

## [2.0.0] - 2023-08-16 - **MAJOR UPDATE**

### Added

- Added "Append" button to create an offshoot from the current journal based on the user's request. This lets you ask for things like a new sibling
  for an NPC or a backstory or family. Also good for populating a town with locations or people or events. Endless use cases!
- Added many more localization strings
- Improved logging with optional ui notifications

### Fixed

- Huge code cleanup and reorganization
- Divided out some reusable code into the new [Random Procedural Core](https://gitlab.com/rpgm-tools/rp-core) module
- Greatly improved homebrew generation and schema management - these should now respect the game system you are using far more reliably. You will
  still see better results if you make the appropriate modifications to the schema or create your own custom schema
- Simplified and moved all settings to the core module
- Users on the Foundry app can now use the Amend and Append buttons in journal entries

## [1.2.0] - 2023-08-08

### Added

- Added a few more homebrew schemas and tweaked how customizing them works
- Added delete button to remove user-added schemas or revert changes
- Added a way to pull gender from the token art filename for those who use randomized token art (Nocturnal ;)
- Improvements to the journal entry buttons

### Fixed

- Improved homebrew AI prompt to try to include stats more often and to try harder to adjust for systems other than D&D 5e
- Removed some unused functions
- Miscellanous bug fixes and improvements

## [1.1.0] - 2023-08-06

### Added

- Enhance button added to Journal entry view to request embellishment from AI
- Amend button added to Journal entry view to request alteration from AI
- Refresh button added to Journal entry view to request regeneration from AI
- Describe This Token button added to Token HUD to request description from AI
- Updates to !h/!home chat command to greatly expand flexibility and structure of created homebrew content, including creating your own homebrew
  schemas
- **Current custom schemas:** Ability, Action, Armor, Artifact, Background, Boss, Building, Class, Condition, CraftingRecipe, Creature, Cult,
  CulturalPractice, Currency, Deity, Disease, Drink, Encounter, Event, Faction, Fauna, Feat, Festival, Flora, Food, Guild, HistoricalEvent, Holiday,
  Item, Landmark, Language, Legend, Location, MagicItem, Mount, Mythology, NPC, Organization, Plane, PlotHook, Poem, Poison, Profession, Prophecy,
  Puzzle, Quest, Race, Relic, Ritual, SecretSociety, Settlement, Shop, Skill, Song, Spell, Subclass, SymbolSigil, Tome, Tradition, Trap, Vehicle,
  Weapon
- Finally got around to adding the localizations

### Fixed

- Miscellanous bug fixes and improvements

## [1.0.1] - 2023-07-31

### Fixed

- Improved chat card coloring to not be overridden by custom themes
- Minor issues squashed

## [1.0.0] - 2023-07-17

### Added

- Initial release with chat commands only, extracted from [Random Procedural Names](https://gitlab.com/rpgm-tools/rp-names/-/blob/main/README.md)
  module

### Fixed

- Nothing yet
