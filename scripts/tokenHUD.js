const rp = CONFIG.rp;

const AI_DESC_ICON_HTML = `<div class="control-icon rp-creations-ai-desc"><i class="fas fa-wand-sparkles"></i></div>`;

const showDescription = (tokenHUD, html) => {
	const aiDescButton = $(AI_DESC_ICON_HTML);

	const chatColors = {
		default: "#FFFFFF",
		description: {
			background: "#EEAADD",
			border: "#881177",
			text: "#881177",
		},
	};

	aiDescButton.attr("title", game.i18n.localize("RP-CREATIONS.TOKENHUD.DESCRIPTION.BUTTON"));
	html.find(".col.left").append(aiDescButton);

	aiDescButton.addClass("pulsate");
	aiDescButton.find("i.fas.fa-wand-sparkles").append('<div class="ai-overlay">AI</div>');

	function extractAiContentFromString(str) {
		let parser = new DOMParser();
		let doc = parser.parseFromString(str, "text/html");
		let aiContentDiv = doc.querySelector(".ai-content");

		return aiContentDiv ? aiContentDiv.innerHTML : null;
	}

	aiDescButton.on("mousedown", async (event) => {
		event.preventDefault();

		const actor = tokenHUD.object.actor;
		const name = tokenHUD.object.name;

		actor.flags.gender = rp.getGender(tokenHUD.object);
		const actorData =
			event.which === 3 ? await rp.getActorData(game.system.id, actor, name, true) : await rp.getActorData(game.system.id, actor, name);

		const payload = {
			model: rp.variables.model,
			messages: [
				{
					role: "system",
					content: `You are an AI GM for TTRPGs (specifically, ${rp.variables.system}) in Foundry. You assess the available data in a JSON object about an actor in a ${rp.variables.genre} setting and generate your own rich, creative, immersive, narrative homebrew description for the actor's background, appearance, demeanor, behavior, abilities, etc., without directly referencing the properties of the provided JSON object. Your response starts with an <h1> tag and is formatted with HTML with inline CSS formatting (though no coloring) to add structure with a tasteful combination of lists, headings, emphasis, and any other elements that can help make your answer look good.  Simple unformatted paragraphs will always be avoided.  Format will be such that it looks good inside a div container with a width ranging from 248px to 800px. You will ignore any properties from the provided object that are nullish, empty, or otherwise not useful for your response. Mentions of artwork creators, such as Forgotten Adventures, in the biography are also irrelevant and will be ignored.`,
				},
				{ role: "user", content: `Generate a description for the following actor:\n\n${JSON.stringify(actorData)}` },
				{ role: "assistant", content: "Description HTML:\n\n" },
			],
			temperature: rp.variables.temperature,
			presence_penalty: 0.2,
			frequency_penalty: 0.2,
			apiKey: rp.variables.apiKey,
			patreonKey: rp.variables.patreonKey,
			clientId: rp.variables.clientId,
		};

		rp.dev("Payload:\n", payload);

		let response = rp.removeImgTags(await rp.callLambdaFunction(payload));
		if (response) {
			rp.dev("Response:\n", response);
			const chatResponse = `<h1>${name}:</h1><p>${response}</p>`;
			let chatData = {
				content: `<div class="ai-content" style="border:2px solid ${chatColors.description.border}; color: ${chatColors.description.text}; background-color: ${chatColors.description.background}; padding: 10px; border-radius: 5px;">${chatResponse}</div>`,
			};

			if (game?.user?.isGM) {
				chatData.type = CONST.CHAT_MESSAGE_TYPES.WHISPER;
				chatData.whisper = [game.user.id];
			}

			const copyText = extractAiContentFromString(chatData.content);

			let chatMessage = await ChatMessage.create(chatData);

			await chatMessage.setFlag("rp-creations", "copyText", copyText);

			let homeID = rp.generateUUID();

			rp.variables.descMap.set(homeID, response);

			const journalButton = `<button style="background-color: ${chatColors.description.background}; color: ${
				chatColors.description.text
			}; border: 1px solid ${
				chatColors.description.border
			}; padding: 2; margin-right: 5px; cursor: pointer; width: auto; line-height: 1;" class="create-journal" data-message-id="${
				chatMessage._id
			}" data-subject="${name}" data-desc-id="${homeID}">${game.i18n.localize("RP-CREATIONS.CHAT.CONTROLS.JOURNAL")}</button>`;
			const deleteButton = `<button style="background-color: ${chatColors.description.background}; color: ${
				chatColors.description.text
			}; border: 1px solid ${
				chatColors.description.border
			}; padding: 2; cursor: pointer; width: auto; line-height: 1;" class="delete-card" data-message-id="${chatMessage._id}">${game.i18n.localize(
				"RP-CREATIONS.CHAT.CONTROLS.DELETE"
			)}</button>`;
			const buttonGroup = `<div style="text-align: center;">${journalButton}${deleteButton}</div>`;

			await chatMessage.update({
				content: `${chatMessage.content}${buttonGroup}`,
			});

			ui.chat.scrollBottom();
		}
	});
};

export { showDescription };
