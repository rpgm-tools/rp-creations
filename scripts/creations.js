const rp = CONFIG.rp;

function removeImgTags(input) {
	return input.replace(/<img.*?\/?>/gi, "");
}

function compileRecordTypes(schemas) {
	return Object.keys(schemas);
}

function getHomebrewSchemaByRecordType(recordType) {
	const schemas = rp.mergeSchemas();
	const schemasArray = Object.values(schemas); // Convert the object values into an array

	const lowerCaseRecordType = recordType.toLowerCase();

	const schemaObj = schemasArray.find((schema) => schema.recordType.toLowerCase() === lowerCaseRecordType);

	return schemaObj;
}

function getHomebrewOptions(responseLength, responseFormat) {
	return {
		responseFormat: responseFormat || "HTML",
		responseLength: responseLength || "medium",
	};
}

async function backUpJournal(docId) {
	const doc = game.journal.get(docId);

	if (!doc) {
		rp.warn(`Document with ID ${docId} not found.`);
		return;
	}

	if (doc.name.includes(" (Backup")) {
		rp.log("Journal is already a backup.");
		return;
	}

	let backupCount = 1;
	const oldName = doc.name;
	const backupRegex = new RegExp(`^${oldName.replace(/[.*+?^${}()|[\]\\]/g, "\\$&")} \\(Backup (\\d+)\\)$`);

	game.journal.forEach((entry) => {
		const match = entry.name.match(backupRegex);
		if (match) {
			const count = parseInt(match[1]);
			if (count >= backupCount) {
				backupCount = count + 1;
			}
		}
	});

	const newName = oldName + ` (Backup ${backupCount})`;
	await doc.update({ name: newName });

	rp.log(`Document renamed to ${newName}`);
}

async function createJournalEntry(name, content) {
	let folder = game.folders.find((f) => f.name === "RP-Creations" && f.type === "JournalEntry");

	if (!folder) {
		folder = await Folder.create({
			name: "RP-Creations",
			type: "JournalEntry",
			parent: null,
		});
	}

	return JournalEntry.create({
		name: name,
		content: content,
		folder: folder.id,
	});
}

export { removeImgTags, compileRecordTypes, getHomebrewSchemaByRecordType, getHomebrewOptions, backUpJournal, createJournalEntry };
