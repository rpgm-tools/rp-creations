const rp = CONFIG.rp;
import { showDescription } from "./tokenHUD.js";

const registerRenderTokenHUDHook = () => {
	Hooks.on("renderTokenHUD", (tokenHUD, html, app) => {
		if (game.user.isGM) {
			showDescription(tokenHUD, html);
		}
	});
};

function registerRenderJournalSheetHook() {
	Hooks.on("renderJournalSheet", (app, html, data) => {
		const titleInput = html.find('input.title[name="name"]');

		const commonButtonStyle = `
    padding: 10px 15px;
    background-color: #6633cc;
    color: #fcfaff;
    border: none;
    border-radius: 4px;
    cursor: pointer;
    transition: background-color 0.3s ease;
    margin-right: 5px;
`;

		const enhanceButton = $("<button>")
			.addClass("rp-creations-enhance-button")
			.html(`<i class="fas fa-magic fa-fw fa-lg"></i> ${game.i18n.localize("RP-CREATIONS.HOOKS.RENDERJOURNALSHEET.BUTTON_ENHANCE")}`)
			.attr("style", commonButtonStyle);

		const amendButton = $("<button>")
			.addClass("rp-creations-amend-button")
			.html(`<i class="fas fa-edit fa-fw fa-lg"></i> ${game.i18n.localize("RP-CREATIONS.HOOKS.RENDERJOURNALSHEET.BUTTON_AMEND")}`)
			.attr("style", commonButtonStyle);

		const appendButton = $("<button>")
			.addClass("rp-creations-append-button")
			.html(`<i class="fas fa-plus-circle fa-fw fa-lg"></i> ${game.i18n.localize("RP-CREATIONS.HOOKS.RENDERJOURNALSHEET.BUTTON_APPEND")}`)
			.attr("style", commonButtonStyle);

		const refreshButton = $("<button>")
			.addClass("rp-creations-refresh-button")
			.html(`<i class="fas fa-sync fa-fw fa-lg"></i> ${game.i18n.localize("RP-CREATIONS.HOOKS.RENDERJOURNALSHEET.BUTTON_REFRESH")}`)
			.attr("style", commonButtonStyle);

		const buttonContainer = $("<div>").addClass("rp-creations-button-container");

		buttonContainer.append(enhanceButton, amendButton, appendButton, refreshButton);

		titleInput.before(buttonContainer);

		const journalId = app.object.id;
		const pageIndex = app.pageIndex;

		enhanceButton.on("click", (event) => {
			enhanceFunction(event).catch((error) => rp.error(error));
		});

		const enhanceFunction = async (event) => {
			const subject = html
				.find('input.title[name="name"]')
				.val()
				.replace(/ \(Backup \d+\)$/, "");
			let body = html.find("section.journal-page-content").html();

			const payload = {
				model: rp.variables.model,
				messages: [
					{
						role: "system",
						content: `Within a ${rp.variables.genre} ${rp.variables.system} context, enhance the user's content, introducing additional details and sections as you see fit, but without using obtuse language. Focus more on providing net new information about aspects that were previously not included. Either maintain the provided HTML formatting or enhance it. Ensure the final response reflects the original text, but with integrated enhancements.`,
					},
					{
						role: "user",
						content: `Expand, enhance, and add rich and interesting details to the following description:\n\n${body}`,
					},
					{ role: "assistant", content: "Revised:\n\n" },
				],
				temperature: rp.variables.temperature,
				presence_penalty: 0,
				frequency_penalty: 0,
				apiKey: rp.variables.apiKey,
				patreonKey: rp.variables.patreonKey,
				clientId: rp.variables.clientId,
			};

			rp.dev("Payload:\n", payload);

			body = await rp.callLambdaFunction(payload);

			rp.dev("Response:\n", body);

			await rp.backUpJournal(journalId);

			const entry = await rp.createJournalEntry(subject, body);

			entry.sheet.render(true);
		};

		amendButton.on("click", (event) => {
			amendFunction(event).catch((error) => rp.error(error));
		});

		const amendFunction = async (event) => {
			const subject = html
				.find('input.title[name="name"]')
				.val()
				.replace(/ \(Backup \d+\)$/, "");
			let body = html.find("section.journal-page-content").html();

			let desiredChanges = await sharedPrompt("amend");
			if (!desiredChanges) return;

			const payload = {
				model: rp.variables.model,
				messages: [
					{
						role: "system",
						content: `Within a ${rp.variables.genre} ${rp.variables.system} context, modify the user's content based on their instructions. Integrate the requested changes, adding or adapting as needed. Preserve or improve the provided HTML formatting. Ensure the final response reflects the original text with the incorporated changes.`,
					},
					{
						role: "user",
						content: `Make the following changes to the below description: ${desiredChanges}:\n\n${body}`,
					},
					{ role: "assistant", content: "Revised:\n\n" },
				],
				temperature: rp.variables.temperature,
				presence_penalty: 0,
				frequency_penalty: 0,
				apiKey: rp.variables.apiKey,
				patreonKey: rp.variables.patreonKey,
				clientId: rp.variables.clientId,
			};

			rp.dev("Payload:\n", payload);

			body = await rp.callLambdaFunction(payload);

			rp.dev("Response:\n", body);

			await rp.backUpJournal(journalId);

			const entry = await rp.createJournalEntry(subject, body);

			entry.sheet.render(true);
		};

		appendButton.on("click", (event) => {
			appendFunction(event).catch((error) => rp.error(error));
		});

		const appendFunction = async (event) => {
			let h1Content = $(html.find("section.journal-page-content")).find("h1").text();
			const subject = h1Content || html.find('input.title[name="name"]').val() + " (Related Content)";

			let body = html.find("section.journal-page-content").html();

			let desiredAddition = await sharedPrompt("append");
			if (!desiredAddition) return;

			const payload = {
				model: rp.variables.model,
				messages: [
					{
						role: "system",
						content: `Within a ${rp.variables.genre} ${rp.variables.system} context, craft new content related to the provided description based on user instructions. Ensure to name any new characters, places, or entities appropriately. The HTML formatting of your output should be consistent with the provided content's formatting.`,
					},
					{
						role: "user",
						content: `Generate the following new content as it relates to the below description: ${desiredAddition}:\n\n${body}`,
					},
					{ role: "assistant", content: "New Content:\n\n" },
				],
				temperature: rp.variables.temperature,
				presence_penalty: 0,
				frequency_penalty: 0,
				apiKey: rp.variables.apiKey,
				patreonKey: rp.variables.patreonKey,
				clientId: rp.variables.clientId,
			};

			rp.dev("Payload:\n", payload);

			body = await rp.callLambdaFunction(payload);

			rp.dev("Response:\n", body);

			const entry = await rp.createJournalEntry(subject, body);

			entry.sheet.render(true);
		};

		refreshButton.on("click", (event) => {
			refreshFunction(event).catch((error) => rp.error(error));
		});

		const refreshFunction = async (event) => {
			const subject = html
				.find('input.title[name="name"]')
				.val()
				.replace(/ \(Backup \d+\)$/, "");

			const messages = [
				{
					role: "system",
					content: `Craft a response using HTML and CSS to organize it with headings, subheadings, lists, emphases of various types, as needed, making it visually appealing. Ensure the content is optimized for containers with widths between 248px and 800px. As an expert GM in TTRPGs, especially ${rp.variables.system}, create detailed descriptions for entities like creatures, items, and places. If the system isn't specified, use ${rp.variables.system}. Assign a suitable gender when detailing a person, if it's unclear. Avoid redundancy. For mundane items, keep the writing simple. For homebrew items, emulate the style of TTRPG source books for ${rp.variables.system}.`,
				},
				{
					role: "user",
					content: `Generate the following, giving it a proper name, if one is not specified, as well as a description:\n\n{request: "${subject.replace(
						/\\"/g,
						'"'
					)}", genre: "${rp.variables.genre}", ${rp.variables.language !== "Default" ? 'language: "' + rp.variables.language + '", ' : ""}system: "${
						rp.variables.system
					}"}`,
				},
				{ role: "assistant", content: `<h1>${subject}:</h1>` },
			];

			const payload = {
				model: rp.variables.model,
				messages: messages,
				temperature: rp.variables.temperature,
				presence_penalty: 0.2,
				frequency_penalty: 0.2,
				apiKey: rp.variables.apiKey,
				patreonKey: rp.variables.patreonKey,
				clientId: rp.variables.clientId,
			};

			rp.dev("Payload:\n", payload);

			let response = await rp.callLambdaFunction(payload);
			if (response) {
				rp.dev("Response:\n", response);
				await rp.backUpJournal(journalId);

				const entry = await rp.createJournalEntry(subject, response);

				entry.sheet.render(true);
			}
		};

		async function sharedPrompt(actionType = "amend") {
			return new Promise((resolve, reject) => {
				let promptText =
					actionType === "amend"
						? game.i18n.localize("RP-CREATIONS.HOOKS.RENDERJOURNALSHEET.DIALOG_PROMPT_AMEND")
						: game.i18n.localize("RP-CREATIONS.HOOKS.RENDERJOURNALSHEET.DIALOG_PROMPT_APPEND");

				let content = `
					<form>
						<div class="form-group">
							<label>${promptText}:</label>
							<textarea name="inputVal" rows="4" cols="50"></textarea>
						</div>
					</form>`;

				const dialog = new Dialog({
					title: game.i18n.localize("RP-CREATIONS.HOOKS.RENDERJOURNALSHEET.DIALOG_TITLE"),
					content: content,
					buttons: {
						ok: {
							label: "OK",
							callback: (html) => {
								let inputValue = html.find('textarea[name="inputVal"]').val();
								if (inputValue.trim()) {
									resolve(inputValue);
								} else {
									resolve(null);
								}
							},
						},
						cancel: {
							label: game.i18n.localize("RP-CREATIONS.HOOKS.RENDERJOURNALSHEET.DIALOG_CANCEL"),
							callback: () => {
								resolve(null);
							},
						},
					},
					default: "ok",
					render: (html) => {
						html.find('textarea[name="inputVal"]').focus();
					},
				});

				dialog.render(true);
			});
		}
	});
}

export { registerRenderTokenHUDHook, registerRenderJournalSheetHook };
