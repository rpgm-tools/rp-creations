let rp;

//Other initializations

Hooks.once("rpCoreReady", async () => {
	rp = CONFIG.rp;
	rp.creations = true;

	const creations = await import("./creations.js");
	Object.assign(rp, creations);

	const chat = await import("./chat.js");
	Object.assign(rp, chat);

	const hooks = await import("./hooks.js");
	hooks.registerRenderTokenHUDHook();
	hooks.registerRenderJournalSheetHook();

	console.log(
		`%cRandom Procedural Creations v${rp.creationsVersion}
	__________________________________________________________
	 ____  ____     ____                _   _                 
	|  _ \\|  _ \\   / ___|_ __ ___  __ _| |_(_) ___  _ __  ___ 
	| |_) | |_) | | |   | '__/ _ \\/ _\` | __| |/ _ \\| '_ \\/ __|
	|  _ <|  __/  | |___| | |  __/ (_| | |_| | (_) | | | \\__ \\
	|_| \\_\\_|      \\____|_|  \\___|\\__,_|\\__|_|\\___/|_| |_|___/
	__________________________________________________________`,
		"color: #7b4ed4; font-weight: bold;"
	);
});

export { rp };
