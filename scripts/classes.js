const rp = CONFIG.rp;

class HomebrewForm extends FormApplication {
	constructor(object, options = {}) {
		super(object, options);
		this.data = options.data;
		this._shouldSubmit = true;
	}

	static get defaultOptions() {
		return mergeObject(super.defaultOptions, {
			id: "homebrew-form",
			title: "Homebrew Form",
			template: "modules/rp-creations/templates/homebrew.html",
			width: 500,
			height: 650,
			resizable: true,
			closeOnSubmit: true,
		});
	}

	getData() {
		const schemas = rp.mergeSchemas();

		const sortedKeys = Object.values(schemas)
			.map((schema) => schema.recordType)
			.sort();

		const defaultRecordType = sortedKeys[0];
		const defaultSchemaData = Object.values(schemas).find((schema) => schema.recordType === defaultRecordType);
		defaultSchemaData.system = rp.variables.system;
		if (!defaultSchemaData.customSchema) {
			defaultSchemaData.customSchema = false;
		}
		const defaultSchema = JSON.stringify(defaultSchemaData, null, 1);

		return {
			sortedKeys: sortedKeys,
			schemas: schemas,
			defaultSchema: defaultSchema,
		};
	}

	activateListeners(html) {
		super.activateListeners(html);

		const schemas = rp.mergeSchemas();

		html.find("#record-type").change((event) => {
			const selectedType = event.target.value;
			const selectedSchemaData = Object.values(schemas).find((schema) => schema.recordType === selectedType);
			const otherSchema = JSON.stringify(
				{
					recordType: html.find("#custom-record-type").val(),
					customSchema: true,
					system: rp.variables.system,
					data: {
						name: { type: "string", required: true },
						description: { type: "string", required: true },
						type: { type: "string", required: true },
					},
				},
				null,
				1
			);

			if (selectedType === "custom") {
				html.find("#custom-record-type").show();
				html.find("#schema").val(otherSchema);
				html.find("#delete-schema-btn").hide();
			} else {
				html.find("#custom-record-type").hide().val("");

				selectedSchemaData.system = rp.variables.system;
				if (!selectedSchemaData.customSchema) {
					selectedSchemaData.customSchema = false;
				}
				const schema = JSON.stringify(selectedSchemaData, null, 1);
				html.find("#schema").val(schema);

				if (selectedSchemaData && selectedSchemaData.customSchema) {
					html.find("#delete-schema-btn").show();
				} else {
					html.find("#delete-schema-btn").hide();
				}
			}
		});

		html.find("#schema").on(
			"input",
			rp.debounce((event) => {
				const textarea = event.target;
				const start = textarea.selectionStart;
				const end = textarea.selectionEnd;

				const fileSchemas = rp.lists.homebrewSchemas.reduce((acc, schema) => {
					acc[schema.recordType] = schema;
					return acc;
				}, {});

				let schemaStr = textarea.value;
				let schema;

				try {
					schema = JSON.parse(schemaStr);
				} catch (error) {
					return;
				}

				let isSchemaFromFile = false;
				for (let key in fileSchemas) {
					if (rp.deepEqual(schema, fileSchemas[key])) {
						isSchemaFromFile = true;
						break;
					}
				}

				const oldValueIndex = schemaStr.indexOf('"customSchema":');
				let offset = 0;

				if (isSchemaFromFile) {
					schema.customSchema = false;
					offset = oldValueIndex !== -1 ? -17 : 0;
				} else if (!schema.customSchema) {
					schema.customSchema = true;
					offset = oldValueIndex === -1 ? 17 : 0;
				}

				schemaStr = JSON.stringify(schema, null, 1);
				$(textarea).val(schemaStr);

				if (start > oldValueIndex) {
					textarea.setSelectionRange(start + offset, end + offset);
				} else {
					textarea.setSelectionRange(start, end);
				}
			}, 2000)
		);

		html.find("#delete-schema-btn").click(async (event) => {
			event.preventDefault();

			const selectedType = html.find("#record-type").val();

			rp.log("Deleting custom schema for " + selectedType + ":\n", mergedSchemas[selectedType]);

			delete mergedSchemas[selectedType];

			if (rp.lists.homebrewSchemas[selectedType]) {
				mergedSchemas[selectedType] = rp.lists.homebrewSchemas[selectedType];
			}

			await game.settings.set("rp-core", "settingsHomebrewSchemas", mergedSchemas);

			await this.render();

			html.find("#record-type").val(selectedType);
		});

		html.find("#custom-record-type").on("input", (event) => {
			const otherSchema = JSON.stringify(
				{
					recordType: rp.toTitleCase(html.find("#custom-record-type").val()),
					customSchema: true,
					system: "all",
					data: {
						name: { type: "string", required: true },
						description: { type: "string", required: true },
						type: { type: "string", required: true },
					},
				},
				null,
				1
			);

			html.find("#schema").val(otherSchema);
		});

		html.find("form").submit(async (event) => {
			if (this._shouldSubmit) {
				event.preventDefault();

				const schemaStr = event.currentTarget.elements["schema"].value;

				let schema;

				try {
					schema = JSON.parse(schemaStr);
				} catch (error) {
					rp.error(game.i18n.localize("RP-CREATIONS.CLASSES.HOMEBREWFORM.SCHEMA-ERROR"), true);
					return;
				}

				const formData = new FormData(event.currentTarget);
				formData.set("schema", JSON.stringify(schema));
				await this._updateObject(event, formData);
				this.close();
			}
		});

		html.find(".window-close, .close").click(() => {
			this._shouldSubmit = false;
		});

		$(document).keyup((e) => {
			if (e.key === "Escape") {
				this._shouldSubmit = false;
			}
		});
	}

	async _updateObject(event, formData) {
		const recordType = rp.toTitleCase(formData["custom-record-type"]) || formData["record-type"];
		const prompt = formData["prompt"];
		const schema = await JSON.parse(formData["schema"]);

		const schemas = rp.mergeSchemas();

		rp.log("Creating custom schema for " + recordType + ":\n", schema);

		schemas[recordType] = schema;

		await game.settings.set("rp-core", "settingsHomebrewSchemas", schemas);

		await rp.homebrewArg(recordType, prompt, schema, this.data);
	}

	async _onSubmit(event, { updateData = null, preventClose = false } = {}) {
		if (this._shouldSubmit) {
			return super._onSubmit(event, { updateData, preventClose });
		}
		event.preventDefault();
		return false;
	}
}

export { HomebrewForm };
