const rp = CONFIG.rp;
import { HomebrewForm } from "./classes.js";

async function homebrew(prompt, messageData) {
	const messages = [
		{
			role: "system",
			content: `Provide a response using HTML with inline CSS to ensure it's aesthetically pleasing, including headings, subheadings, lists, emphases of various types, as needed. The format should be optimized for containers with a width between 248px and 800px. As a GM with extensive knowledge of TTRPGs, especially ${rp.variables.system}, create immersive descriptions for creatures, items, places, and other homebrew content based on the user's prompt. If the system isn't specified, default to ${rp.variables.system}. When detailing a person and their gender isn't obvious, choose the most suitable one. Avoid repeating the request in your answer. Your writing should match the typical TTRPG source book style for ${rp.variables.system}. Be precise and ensure any mechanics or stats provided align with the game's rules. Avoid mentioning the game system or the term "homebrew." Generate appropriate names for creations unless specified.`,
		},
		{
			role: "user",
			content: `Generate the following, ensuring it has a suitable name and detailed description:\n\n{request: "${prompt}", setting_genre: "${
				rp.variables.genre
			}", ${rp.variables.language !== "Default" ? 'description_output_language: "' + rp.variables.language + '", ' : ""}game_system: "${
				rp.variables.system
			}"}`,
		},
		{ role: "assistant", content: `<h1>${rp.toTitleCase(prompt)}:</h1>\n<br />\n` },
	];

	const payload = {
		model: rp.variables.model,
		messages: messages,
		temperature: 1.2,
		presence_penalty: 0.2,
		frequency_penalty: 0.2,
		apiKey: rp.variables.apiKey,
		patreonKey: rp.variables.patreonKey,
		clientId: rp.variables.clientId,
	};

	rp.dev("Payload:\n", payload);

	let response = rp.removeImgTags(await rp.callLambdaFunction(payload));

	if (response) {
		rp.dev("Response:\n", response);
		const chatResponse = `<h1>${rp.toTitleCase(prompt)}:</h1><p>${response}</p>`;
		let chatData = {
			content: `<div class="ai-content" style="border:2px solid ${rp.chatColors.homebrew.border}; color: ${
				rp.chatColors.homebrew.text
			}; background-color: ${rp.chatColors.homebrew.background}; padding: 10px; border-radius: 5px;">${chatResponse}</div>
                <p style="color: ${rp.chatColors.homebrew.text};"><b>!help</b> ${game.i18n.localize("RP-CREATIONS.CHAT.FOOTER.HELP")}</p>`,
		};

		chatData.type = CONST.CHAT_MESSAGE_TYPES.WHISPER;
		chatData.whisper = [game.user.id];

		const copyText = rp.extractAIContentFromString(chatData.content);

		let chatMessage = await ChatMessage.create(chatData);

		await chatMessage.setFlag("rp-core", "copyText", copyText);

		let homeID = rp.generateUUID();

		rp.variables.descMap.set(homeID, response);

		const journalButton = `<button style="background-color: ${rp.chatColors.homebrew.background}; color: ${
			rp.chatColors.homebrew.text
		}; border: 1px solid ${
			rp.chatColors.homebrew.border
		}; padding: 2px; margin-right: 5px; cursor: pointer; width: auto; line-height: 1;" class="create-journal" data-message-id="${
			chatMessage._id
		}" data-subject="${prompt}" data-desc-id="${homeID}">${game.i18n.localize("RP-CREATIONS.CHAT.CONTROLS.JOURNAL")}</button>`;
		const deleteButton = `<button style="background-color: ${rp.chatColors.homebrew.background}; color: ${
			rp.chatColors.homebrew.text
		}; border: 1px solid ${
			rp.chatColors.homebrew.border
		}; padding: 2px; cursor: pointer; width: auto; line-height: 1;" class="delete-card" data-message-id="${chatMessage._id}">${game.i18n.localize(
			"RP-CREATIONS.CHAT.CONTROLS.DELETE"
		)}</button>`;
		const buttonGroup = `<div style="text-align: center;">${journalButton}${deleteButton}</div>`;

		await chatMessage.update({
			content: `${chatMessage.content}${buttonGroup}`,
		});

		ui.chat.scrollBottom();
	}
}

async function homebrewArg(recordType, prompt, schema = null, messageData) {
	schema = rp.getHomebrewSchemaByRecordType(recordType);
	schema.system = rp.variables.system;
	const messages = [
		{
			role: "system",
			content: `As a knowledgeable GM deeply familiar with TTRPGs, particularly ${rp.variables.system}, your task is to invent and generate immersive descriptions for entities like creatures, items, and places, among many others, using HTML with CSS to organize it with headings, subheadings, lists, emphases of various types, as needed. If a prompt is presented without a specified system or mentions a system other than ${rp.variables.system}, adapt your creation to fit ${rp.variables.system}. In cases where the user-provided schema includes properties that are not coherent with ${rp.variables.system}, you have the discretion to either omit them or replace them with properties that align better with this system. When describing individuals and their gender isn't evident, select the most fitting one. Refrain from including any part of the original request in your response. Maintain a simple writing style for ordinary objects. However, when the context demands, your writing should resonate with the narrative tone found in typical TTRPG source books for ${rp.variables.system}. Ensure precision in your descriptions, integrating mechanics and stats when they are relevant and ensuring they comply with the rules of the respective game. Do not make any reference to the system or use the term "homebrew." Unless already provided, always craft a suitable name or title for your creation.`,
		},
		{
			role: "user",
			content: `Generate the following homebrew:\n\n{schema: ${JSON.stringify(schema)}, request: "${rp.toTitleCase(prompt)}", setting_genre: "${
				rp.variables.genre
			}", ${rp.variables.language !== "Default" ? 'description_output_language: "' + rp.variables.language + '", ' : ""}game_system: "${
				rp.variables.system
			}"}`,
		},
		{ role: "assistant", content: `<h1>(${rp.toTitleCase(recordType)}) ${rp.toTitleCase(prompt)}:</h1>\n<br />\n` },
	];

	const payload = {
		model: rp.variables.model,
		messages: messages,
		temperature: 1.2,
		presence_penalty: 0.2,
		frequency_penalty: 0.2,
		apiKey: rp.variables.apiKey,
		patreonKey: rp.variables.patreonKey,
		clientId: rp.variables.clientId,
	};

	rp.dev("Payload:\n", payload);

	let response = rp.removeImgTags(await rp.callLambdaFunction(payload));

	if (response) {
		rp.dev("Response:\n", response);
		const chatResponse = `<h1>(${rp.toTitleCase(recordType)}) ${rp.toTitleCase(prompt)}:</h1><p>${response}</p>`;
		let chatData = {
			content: `<div class="ai-content" style="border:2px solid ${rp.chatColors.homebrew.border}; color: ${
				rp.chatColors.homebrew.text
			}; background-color: ${rp.chatColors.homebrew.background}; padding: 10px; border-radius: 5px;">${chatResponse}</div>
                <p style="color: ${rp.chatColors.homebrew.text};"><b>!help</b> ${game.i18n.localize("RP-CREATIONS.CHAT.FOOTER.HELP")}</p>`,
		};

		chatData.type = CONST.CHAT_MESSAGE_TYPES.WHISPER;
		chatData.whisper = [game.user.id];

		const copyText = rp.extractAIContentFromString(chatData.content);

		let chatMessage = await ChatMessage.create(chatData);

		await chatMessage.setFlag("rp-core", "copyText", copyText);

		let homeID = rp.generateUUID();

		rp.variables.descMap.set(homeID, response);

		const journalButton = `<button style="background-color: ${rp.chatColors.homebrew.background}; color: ${
			rp.chatColors.homebrew.text
		}; border: 1px solid ${
			rp.chatColors.homebrew.border
		}; padding: 2px; margin-right: 5px; cursor: pointer; width: 150px; line-height: 1;" class="create-journal" data-message-id="${
			chatMessage._id
		}" data-subject="${prompt}" data-desc-id="${homeID}">${game.i18n.localize("RP-CREATIONS.CHAT.CONTROLS.JOURNAL")}</button>`;
		const deleteButton = `<button style="background-color: ${rp.chatColors.homebrew.background}; color: ${
			rp.chatColors.homebrew.text
		}; border: 1px solid ${
			rp.chatColors.homebrew.border
		}; padding: 2px; cursor: pointer; width: 80px; line-height: 1;" class="delete-card" data-message-id="${chatMessage._id}">${game.i18n.localize(
			"RP-CREATIONS.CHAT.CONTROLS.DELETE"
		)}</button>`;
		const buttonGroup = `<div style="text-align: center;">${journalButton}${deleteButton}</div>`;

		await chatMessage.update({
			content: `${chatMessage.content}${buttonGroup}`,
		});

		ui.chat.scrollBottom();
	}
}

async function homebrewDialog(data) {
	const formApp = new HomebrewForm({}, { data: data });
	formApp.render(true);
}

async function gptPrompt(prompt, messageData) {
	const messages = [
		{
			role: "system",
			content: `Craft your response using HTML and CSS to organize it with headings, subheadings, lists, emphases of various types, as needed. The response should fit a fixed width of 249px. Images and image links aren't supported. As a TTRPG expert, especially in ${rp.variables.system}, answer any queries or complete statements related to TTRPGs. When the system isn't mentioned, default to ${rp.variables.system}. Your responses simply provide a clear answer and do not make any reference to yourself or your abilities, knowledge, or role. Any mention the game system will use the full name instead of the abbreviation.`,
		},
		{
			role: "user",
			content: prompt,
		},
		{ role: "assistant", content: `<h1>${rp.toTitleCase(prompt)}:</h1>` },
	];

	const payload = {
		model: rp.variables.model,
		messages: messages,
		temperature: 0.3,
		presence_penalty: 0.1,
		frequency_penalty: 0.1,
		apiKey: rp.variables.apiKey,
		patreonKey: rp.variables.patreonKey,
		clientId: rp.variables.clientId,
	};

	rp.dev("Payload:\n", payload);

	let response = rp.removeImgTags(await rp.callLambdaFunction(payload));

	if (response) {
		rp.dev("Response:\n", response);
		const chatResponse = `<p><b>${rp.toTitleCase(prompt)}</b>:</p><p>${response}</p>`;
		let chatData = {
			content: `<div style="border:2px solid ${rp.chatColors.gpt.border}; color: ${rp.chatColors.gpt.text}; background-color: ${
				rp.chatColors.gpt.background
			}; padding: 10px; border-radius: 5px;">${chatResponse}</div>
                <p style="color: ${rp.chatColors.gpt.text};"><b>!help</b> ${game.i18n.localize("RP-CREATIONS.CHAT.FOOTER.HELP")}</p>`,
		};

		chatData.type = CONST.CHAT_MESSAGE_TYPES.WHISPER;
		chatData.whisper = [game.user.id];

		const copyText = rp.extractAIContentFromString(chatData.content);

		let chatMessage = await ChatMessage.create(chatData);

		await chatMessage.setFlag("rp-core", "copyText", copyText);

		const deleteButton = `<button style="background-color: ${rp.chatColors.gpt.background}; color: ${rp.chatColors.gpt.text}; border: 1px solid ${
			rp.chatColors.gpt.border
		}; padding: 2; cursor: pointer; width: auto; line-height: 1;" class="delete-card" data-message-id="${chatMessage._id}">${game.i18n.localize(
			"RP-CREATIONS.CHAT.CONTROLS.DELETE"
		)}</button>`;
		const buttonGroup = `<br><div style="text-align: center;">${deleteButton}</div>`;

		await chatMessage.update({
			content: `${chatMessage.content}${buttonGroup}`,
		});

		ui.chat.scrollBottom();
	}
}

export { homebrew, homebrewArg, homebrewDialog, gptPrompt };
