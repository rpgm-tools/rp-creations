# Random Procedural Creations (rp-creations) for Foundry VTT

**Current version: 2.0.3**

Generate rich homebrew content with the Random Procedural Creations (rp-creations) module for Foundry VTT!

> **Note:** _Unlimited use of AI features is available to Patreon Supporters_

---

## Features

**Foundry VTT**:

- AI-generated homebrew content via chat commands
- Chatbot assistant for TTRPG queries

**Premium Features (Available to Patreon Supporters)**:

- Unlimited AI-generated homebrew content

> _If you enjoy this tool and would like to support its development, please consider becoming a patron at
> [https://www.patreon.com/RPGMTools](https://www.patreon.com/RPGMTools)_

---

## Homebrew Content Types

- **Default Schemas**: Ability, Action, Armor, Artifact, Background, Boss, Building, Class, Condition, CraftingRecipe, Creature, Cult,
  CulturalPractice, Currency, Deity, Disease, Drink, Encounter, Event, Faction, Fauna, Feat, Festival, Flora, Food, Guild, HistoricalEvent, Holiday,
  Item, Landmark, Language, Legend, Location, MagicItem, Mount, Mythology, NPC, Organization, Plane, PlotHook, Poem, Poison, Profession, Prophecy,
  Puzzle, Quest, Race, Relic, Ritual, SecretSociety, Settlement, Shop, Skill, Song, Spell, Subclass, SymbolSigil, Tome, Tradition, Trap, Vehicle,
  Weapon
- **Create your own!**

---

## Languages Supported

- **Real Languages**: Albanian, Arabic, Bengali, Bulgarian, Chinese, Cockney, Croatian, Czech, Danish, Dutch, English, Estonian, Finnish, French,
  German, Greek, Hebrew, Hindi, Hungarian, Icelandic, Irish, Italian, Japanese, Korean, Latvian, Lithuanian, Malay, Maltese, Norwegian, Persian,
  Polish, Portuguese, Romanian, Russian, Serbian, Slovak, Slovenian, Spanish, Swedish, Thai, Turkish, Vietnamese, Welsh, Yiddish, Zulu

- **Fictional and Made-up Languages (Your mileage may vary)**: Pig Latin, Wingdings, Klingon, Esperanto, Elvish, Dothraki, Sindarin, Quenya, Lojban,
  Na'vi, Interlingua, Ido, Volapük, Laadan, Toki Pona, Aurebesh, Dovahzul, Uruk, Newspeak, Nadsat, Kilrathi, Kryptonian, Barsoomian, Gallifreyan,
  Gunganese, High Valyrian, Huttese, Jawaese, Mando'a, Old Tongue, Parseltongue, R'lyehian

- **Add your own languages and see what the AI comes up with!**

---

## Installation

1. In Foundry VTT, go to the Setup menu, then click "Add-on Modules" and "Install Module". Search for "Random Procedural Creations" and click
   "Install".
2. Alternatively, paste the URL to the module.json file into the "Manifest URL" field in the Install Module screen of Foundry:
   `https://gitlab.com/rpgm-tools/rp-creations/-/raw/main/module.json`
3. As a last resort, you can download the zip file from the
   [module's GitLab repository](https://gitlab.com/rpgm-tools/rp-creations/-/archive/main/rp-creations-main.zip), extract the contents to the
   `modules` folder in your Foundry VTT data directory, and enable the RP-Names module in your world's module management settings.

---

## Systems Supported

The Foundry module has been tested and is compatible with the following systems, thought should work with many others:

- Dungeons & Dragons 5e
- Pathfinder 2e
- Worlds Without Number
- SWADE
- Warhammer Fantasy RolePlay 4e
- Dungeons & Dragons 4e
- Dungeons & Dragons 3.5e
- Call of Cthulhu 7e
- Shadowrun 5e
- Twilight: 2000 4e
- Crucible (coming soon!)

---

## Usage

- Chat commands (These are available for supporters, as they require the use of AI):
  - Generates a random homebrewed creature|item|place|race|shop - this is so versatile, but remember, AI is not yet perfect
    - **!h** or **!home** + **description of the desired homebrew**
      - Example: **!h** - Open the Homebrew Generator window to select a schema to use
      - Example: **!h organization secretive orc spy organization** - Generates a secretive orc spy organization
      - Example: **!h location logging village near Neverwinter** - Generates a logging village near Neverwinter
      - Example: **!h plothook volcano** - Generates a plothook about a volcano
  - Queries ChatGPT for a response - ChatGPT built into Foundry. I know, others have done it, but hopefully not as well as I plan to. It is targeted
    to TTRPG topics, and focused on whatever system you are using
    - **!g** or **!gpt** + **a ChatGPT request**
      - Example: **!g A goblin walks into a bar and**
      - Example: **!g What should a player roll to convince a peasant that he or she is actually the monarch?**
      - Example: **!g CR for goblin chief**

---

## Known Issues

- None

---

## Incompatibilities

- None

---

## Reporting Issues and Feature Requests

Please report any issues or request new features on the [module's GitLab issues page](https://gitlab.com/rpgm-tools/rp-creations/-/issues). Include as
much information as you can about your configuration. Suggestions are welcome.

---

## License

This project is licensed under the MIT License. See the [LICENSE](./LICENSE) file for details.

---

## Acknowledgments

- Thank you to the Foundry VTT community for all the wonderful ideas and enthusiasm.
